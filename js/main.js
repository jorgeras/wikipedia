(function () {
  // Global variables to the scope of this script, they simplify the function parameters.
  var wikiForm = document.getElementById('w-form');
  var wikiToc = document.getElementById('w-toc');
  var wikiLanguageSelector = document.getElementById('w-language');
  var wikiTitle = '';
  var wikiLanguage = '';

  // Pseudo-constant
  var LANGUAGES = [{
    code: 'ar',
    name: 'العربية',
    rtl: true
  }, {
    code: 'bpy',
    name: 'বিষ্ণুপ্রিয়া মণিপুরী'
  }, {
    code: 'el',
    name: 'Ελληνικά'
  }, {
    code: 'en',
    name: 'English',
    selected: true
  }, {
    code: 'es',
    name: 'Español'
  }, {
    code: 'he',
    name: 'עברית',
    rtl: true
  }, {
    code: 'ja',
    name: '日本語'
  }, {
    code: 'ko',
    name: '한국어'
  }, {
    code: 'ur',
    name: 'اردو',
    rtl: true
  }, {
    code: 'zh',
    name: '中文'
  }];

  // Pseudo-constant
  var RTL_CLASSNAME = 'w-rtl';

  // Attach event listener so that the target function is triggered when the user submits the form
  wikiForm.addEventListener('submit', fetchWikipediaArticle, false);

  // Initialize form
  populateLanguageSelector();

  /**
   * Automates the language selector generation, so that we have the RTL flag easily available in code.
   */
  function populateLanguageSelector() {
    for (var i in LANGUAGES) {
      var lang = LANGUAGES[i];

      var option = document.createElement('option');

      option.text = lang.name;
      option.value = lang.code;
      option.selected = lang.selected || false;

      if (lang.rtl) {
        option.className = RTL_CLASSNAME;
      }

      wikiLanguageSelector.add(option);
    }
  }

  /**
   * Entry point, will react to the user submitting the form.
   * @param evt
   * @returns {boolean}
   */
  function fetchWikipediaArticle(evt) {
    // Avoid form being automatically submitted, which would refresh the page
    evt.preventDefault();

    wikiTitle = document.getElementById('w-title').value.replace(/ /g, '_');
    wikiLanguage = wikiLanguageSelector.value;

    requestFromWikipedia();

    return false;
  }

  /**
   * Prepares and executes an AJAX request based on XMLHttpRequest (Fetch API is not yet supported in IE11).
   */
  function requestFromWikipedia() {
    // Update title to requirements
    var fetchUrl = `https://${wikiLanguage}.wikipedia.org/api/rest_v1/page/metadata/${wikiTitle}?redirect=false`;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = onResponseFromWikipedia.bind(xhttp);
    xhttp.open('GET', fetchUrl, true);
    xhttp.setRequestHeader('Accept', 'application/json; charset=utf-8; profile="https://www.mediawiki.org/wiki/Specs/Metadata/1.2.0');
    xhttp.setRequestHeader('Api-User-Agent', 'jorge.albaladejo.pomares@gmail.com');
    xhttp.send();
  }

  /**
   * Processes response from Wikipedia and manages errors, if any.
   */
  function onResponseFromWikipedia() {
    if (this.readyState !== 4) {
      // Response not yet ready. Maybe we should implement a timeout to let the user know if the server is down?
      return;
    }

    var response = JSON.parse(this.response);

    if (this.status !== 200) {
      return renderError(this.status, response.title);
    }

    if (!response || !response.toc || !response.toc.entries || !Array.isArray(response.toc.entries)) {
      return renderError(this.status, JSON.stringify(response));
    }

    // For debugging purposes, feel free to remove in production
    console.log(response);

    renderWikipediaToc(response);
  }

  function renderError(status, message) {
    wikiToc.innerHTML = `<p class="w-error"><strong>${status}</strong> - ${message}</p>`;
    // Remove the RTL class if it was added before.
    wikiToc.className = '';
  }

  /**
   * Given a response from Wikipedia, will produce an HTML code and inject it into the placeholder HTML tag.
   * @param response
   */
  function renderWikipediaToc(response) {
    var targetUrl = `https://${wikiLanguage}.wikipedia.org/wiki/${wikiTitle}#`;

    var html = '<ul>';

    var currentLevel = 1;

    // Parse the data structure sequentially to build a nested <ul> HTML tree.
    for (var i in response.toc.entries) {
      var entry = response.toc.entries[i];

      var entryHtml = `<a href="${targetUrl + entry.anchor}" target="_blank" title="${entry.title}">${entry.number} ${entry.html}</a>`;

      // Open a nested inner level
      if (entry.level > currentLevel) {
        html += `<ul><li>${entryHtml}`;
      }
      // Close an open nested inner level
      else if (entry.level < currentLevel) {
        html += `</li></ul><li>${entryHtml}`;
      }
      // Open a new entry at the current level
      else {
        html += `<li>${entryHtml}`;
      }

      currentLevel = entry.level;
    }

    // Close any nested inner levels that may have been left open
    for (var level = currentLevel; level > 1; level--) {
      html += '</li></ul>';
    }

    // Finally, update the TOC
    wikiToc.innerHTML = html;
    wikiToc.className = isLanguageRtl() ? RTL_CLASSNAME : '';
  }

  /**
   * Reads through the available language list to identify whether the selected one is right-to-left.
   * @returns {boolean}
   */
  function isLanguageRtl() {
    for (var i in LANGUAGES) {
      var lang = LANGUAGES[i];

      if (lang.code === wikiLanguage && lang.rtl) {
        return true;
      }
    }

    return false;
  }

})();
